;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

;; (setq init-file-debug t)

;;  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.2")
;; https://irreal.org/blog/?p=8243


(if (eq system-type 'windows-nt)
    ;; On Windows I'm getting gnupgp errors while it is checking the signature for
    ;; adaptive-wrap. Signatures are great and all, but if it won't work unless I
    ;; install or configure gnupgp then it is crap. The software should install and
    ;; if they want to enhance security, great, but not at the expensive of breaking
    ;; the product. How can I recommend Spacemacs to others if every upgrade turns
    ;; into smoking crater requiring me to delete everything, screw around for an
    ;; hour on Google, etc.?
    (setq package-check-signature nil))

(defvar local/this-box-is-slow
  (file-exists-p "~/.spacemacs.d/.this-box-is-slow")
  "If t, disable some packages and features.

My work Windows box is *so* slow because of all the spyware^K^K^K^K^K^K^K
security software that there are lots of tools I can't even use.  (I can barely
use magit.)  I use a file named .this-box-is-slow to identify it and disable a
bunch of stuff.  (I also have fast Windows VMs that I don't want to disable
things for, so I can't just use the OS.)")


(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."

  ;; Use local/this-box-is-slow and the OS type to build the Spacemacs:
  ;;
  ;; * layers    - layers we want loaded
  ;; * packages  - packages we want loaded not included by layers
  ;; * excludes  - packages we want excluded normally included by layers

  (setq layers
        '(
          csv
          helpful                       ; prettier help documents
          emacs-lisp
          (git :variables git-magit-status-fullscreen t)
          html
          (ibuffer :variables ibuffer-group-buffers-by 'projects)
          ivy
          (javascript :variables javascript-backend (if local/this-box-is-slow nil 'tern))
          markdown
          (python :variables
                  python-fill-column 95)
          spell-checking
          sql
          (syntax-checking :variables syntax-checking-enable-tooltips nil)
          (treemacs :variables treemacs-use-icons-dired nil)
          yaml
          deft
          org
          theming
          )
        )

  (setq packages
        '(
          rg
          graphql-mode
          noccur  ; easy multi-occur; great for changes across multiple files
          color-identifiers-mode
          rainbow-mode
          edit-indirect
          org-drill
          poporg
          ))

  (setq excludes
        '(
          gnus
          helm                        ; I use ivy instead
          vi-tilde-fringe
          importmagic                 ; I don't have it installed and the messages are annoying
          importjsd
          magit-gitflow
          magit-svn                   ; I don't know why this gets installed
          sql-indent                  ; I have my own indentation format that is very simple
          ))

  (when (eq system-type 'darwin)
    (setq packages
          (append packages
                  '(
                    reveal-in-osx-finder
                    ob-sql-mode         ; easier pgsql in org files
                    ))))

  (if (not local/this-box-is-slow)
      ;; Fast
      (progn
        (setq layers
              (append layers
                      '(
                        (auto-completion :variables
                                         auto-completion-return-key-behavior nil
                                         auto-completion-tab-key-behavior 'complete
                                         auto-completion-enable-snippets-in-popup t)
                        graphviz
                        (shell :variables shell-default-shell 'shell)
                        yaml
                        systemd
                        )
                      ))
        (setq packages
              (append packages
                      '(
                        ;;  ob-sql-mode
                        )))
        )
    ;; Slow
    (message "Skipping some layers due to .this-box-is-slow file")
    )

  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; If non-nil layers with lazy install support are lazy installed.
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers layers

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   ;; To use a local version of a package, use the `:location' property:
   ;; '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages packages

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages excludes

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives nil

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style '(vim :variables
                                    vim-style-visual-feedback t
                                    vim-style-ex-substitute-global t
                                    vim-style-remap-Y-to-y$ t)

   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner nil

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists nil

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(
                         modus-vivendi
                         modus-operandi
                         zenburn
                         )
   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator rounded :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts.
   ;;
   ;; * Anka Coder: https://github.com/fitojb/anka-coder-fonts & https://fontlibrary.org/en/font/anka-coder
   ;; * Hack: https://sourcefoundry.org/hack (no light version, though)
   ;;   https://gitlab.com/protesilaos/hack-font-mod
   ;; * Source Code Pro
   ;; * Inconsolata
   ;; * Input: http://input.fontbureau.com/download (recommend weight light)
   ;; * Office Code Pro: https://github.com/nathco/Office-Code-Pro (recommend weight light)
   ;;   ("Office Code Pro" :size 18 :width normal :weight light)
   ;; * Noto Mono and Sans Mono: https://www.google.com/get/noto
   ;; * https://github.com/microsoft/cascadia-code
   ;; * https://www.typography.com/fonts/operator/styles/operatormonoscreensmart
   dotspacemacs-default-font '(
                               ;; macOS
                               ("Operator Mono SSm Lig Light" :size 16)

                               ;; On macOS 11 beta, the light version no longer shows up in
                               ;; Emacs, but does in the rest of the OS.
                               ("Operator Mono SSm Lig" :size 16)
                               ;; Windows
                               ;;  ("-outline-Operator Mono SSm Lig Book-light-normal-normal-mono-18-*-*-*-c-*-iso8859-1"
                               ;;   :size 18)
                               ("-outline-Operator Mono SSm Lig Light-light-normal-normal-mono-18-*-*-*-c-*-iso8859-1"
                                :size 18)
                               )

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 30

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location nil

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state t

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay (if (eq system-type 'windows-nt) 2.0 0.4)

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling nil

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but lines are only visual lines are counted. For example, folded lines
   ;; will not be counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers 'visual

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server t

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'changed

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."

  (when (eq system-type 'darwin)
    ;; On Mac make the Cmd key Meta.  It is much more comfortable to use than Option and has
    ;; the added benefit of being in the same place as Alt on Windows keyboards making it
    ;; easier to switch back and forth.
    ;;
    ;; I've set this early so I can use the keyboard if initialization aborts during package
    ;; loading.
    (setq mac-option-key-is-meta nil
          mac-command-key-is-meta t
          mac-command-modifier 'meta
          mac-option-modifier 'hyper
          ;; When double-clicking a file in Finder, open it in the existing emacs window - do
          ;; not create a new window (frame).
          ns-pop-up-frames nil
          ns-use-thin-smoothing t))

  (setq modus-vivendi-theme-rainbow-headings t
        modus-operandi-theme-rainbow-headings t
        modus-vivendi-theme-subtle-diffs t
        modus-vivendi-theme-faint-syntax t)

  ;; This has to be done before the Zenburn theme is loaded.
  (setq zenburn-override-colors-alist
        '(("zenburn-bg" . "#111111")))

  (setq theming-modifications
        '(
          (zenburn

           ;; I tried applying the colors by name using:
           ;;
           ;;   (magit-diff-added
           ;;     : background (assoc-default "zenburn-green-5" zenburn-default-colors-alist))
           ;;
           ;; but it didn't work.  The assoc-default call returns the string color definition,
           ;; but it somehow doesn't take affect.  I've hardcoded the colors but mostly used
           ;; the original palette values.

           ;; Since I darkened the normal background, it is difficult to see the visual mode
           ;; selection (region).  Lighten it from 2b to 2f.  Not too light or the text will be
           ;; difficult to read.
           (region :background "#2f2f2f")

           (fixed-pitch :family nil)

           (org-block :background nil)
           ;; Zenburn sets the background to something pretty light and distracting.

           ;; I don't care for italics in code, even in the comments.  Operator Mono has
           ;; beautiful italics, but I find it slows me down.
           (font-lock-comment-face :weight light :slant normal)
           (font-lock-comment-delimiter-face :weight light :slant normal)
           (font-lock-builtin-face :weight light :slant normal)
           (font-lock-keyword-face :weight light :slant normal)
           (font-lock-constant-face :weight light :slant normal)

           ;; The magit highlight is too light.
           (magit-section-highlight :background "#2b2b2b")

           ;; The highlight version is where the cursor is and it is much more important that
           ;; it be readable.  I've set them both to the same color for now since you can tell
           ;; which one you are in because the context background changes too.
           (magit-diff-added :background "#2F4F2F")
           (magit-diff-added-highlight :background "#2F4F2F")

           (magit-diff-removed :background "#6C3333")
           (magit-diff-removed-highlight :background "#6C3333")

           (magit-diff-context :background "#2B2B2B" :foreground "gray70")
           (magit-diff-context-highlight :background "#111111")

           ;; Flatten the modeline and put a box around it (again, inspired from the Modulo
           ;; themes).

           ;; Now that I'm trying Operator Mono, I'd like to try some italics in the mode line
           ;; or anywhere I don't need to read very often.  Maybe some org-mode headers.
           (mode-line :slant italic :box (:line-width 1 :color "#8fb28f")
                      :background "#111111")
           (mode-line-inactive :slant italic :weight light :background "#111111"
                               :box (:line-width 1 :color: "#2b2b2b")
                               )
           (mode-line-buffer-id-inactive :slant italic :weight light)
           (powerline-active0 :background "#111111")
           (powerline-active1 :background "#2b2b2b")
           (powerline-active2 :background "#111111")
           (powerline-inactive2 :background "#2b2b2b")

           ;; Make the fringe and line number background match the rest of the background to
           ;; blend them.
           (line-number :inherit default :background nil)
           (line-number-current-line :inherit default :background nil)
           (fringe :inherit 'default)

           ;; Ivy - copying Modus which puts a nice box around the current item instead of
           ;; underlining it.  The selected line in the minibuffer and in the regular buffer.
           ;;  (ivy-current-match :box (:line-width 1 :color "#DCDCCC")
           ;;                     :background "#494949" :underline nil :weight normal)
           ;;  (swiper-line-face :box (:line-width 1 :color "#DCDCCC")
           ;;                    :background "#494949" :underline nil :weight normal)
           )
          (
           modus-operandi
           ;; This is the light Modus theme.  I find having a gray stripe for the line numbers
           ;; distracting.  Make them the same white as the editor background.

           (line-number :background "white" :foreground "gray39")
           (line-number-current-line :background "white" :foreground "gray39")

           ;; There are already colors highlighting the headings and bold text never looks very
           ;; crisp in Emacs, at least on an LG5K.
           (org-level-1 :weight normal)
           (org-level-2 :weight normal)
           (org-level-3 :weight normal)
           (org-level-4 :weight normal)
           (org-level-5 :weight normal)
           )
          (
           modus-vivendi

           ;; For some reason the italic face has its own color.
           (italic :foreground nil)
           (fixed-pitch :family nil)

           ;; There are already colors highlighting the headings and bold text never looks very
           ;; crisp in Emacs, at least on an LG5K.
           (org-level-1 :weight normal)
           (org-level-2 :weight normal)
           (org-level-3 :weight normal)
           (org-level-4 :weight normal)
           (org-level-5 :weight normal)

           ;; De-emphasize org src delimiters by using the comment face.  If your source blocks
           ;; start with a lot of comments, they will run together, but I think it is worth it.
           (org-block-begin-line :inherit 'font-lock-comment-face :background nil :foreground nil)
           (org-block-end-line :inherit 'font-lock-comment-face :background nil :foreground nil)
           )
          ))

  ;; Stuff that only applies to the current computer is stored in local-init.el
  ;; and is not checked in.  (It is in .gitignore.)  Since it is not checked in,
  ;; this is also where you'd store confidential items.

  (let ((local "~/.spacemacs.d/local-init.el"))
    (if (file-exists-p local)
        (load-file local)))
  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

  ;; (setq custom-unlispify-tag-names nil)
  ;; I like to use customize to find variables that can be tweaked, but they are displayed with
  ;; English looking names and I wasn't sure how to determine the variable name.  This setting
  ;; will cause it to show the variable names instead, but now that I've looked at it I think
  ;; it was as simple as lowercasing and adding dashes, which I can easily do mentally.

  (mouse-avoidance-mode 'animate)

  (setq frame-resize-pixelwise t)
  ;; When nil, the default, the frame (the outer window) only resizes to multiples
  ;; of the font-size, so it doesn't fill the screen when you maximize, etc.

  (spacemacs/toggle-mode-line-minor-modes-off)
  ;; I rarely toggle anything and can never remember the characters anyway.

  (spacemacs/toggle-highlight-current-line-globally-off)
  ;; You can quickly toggle this back on with SPC t h h

  (setq confirm-kill-processes nil)
  ;; Don't ask to kill processing when exiting.  I often have SQL buffers running.

  (setq comment-padding 2)
  ;; Comment functions should insert two spaces before comments.  The default is 1, but
  ;; Python's PEP8 requires two and I use automatic tools to check it.

  ;; Disable vc backend which might be the cause of the pitiful Windows performance.  I'm using
  ;; magit anyway.
  ;;
  ;; https://github.com/syl20bnr/spacemacs/issues/8462#issuecomment-337417327
  (setq vc-handled-backends nil)

  (require 'ivy-hydra) ; https://oremacs.com/2017/04/09/ivy-0.9.0

  (define-key ivy-switch-buffer-map (kbd "C-k") 'ivy-previous-line)
  ;; Was remapped to kill the buffer.  Spacemacs hadn't overridden yet.
  ;;
  ;; https://github.com/abo-abo/swiper/issues/2141

  (setq ivy-use-selectable-prompt t)
  ;; https://github.com/abo-abo/swiper/issues/933#issuecomment-310638597
  ;; https://github.com/abo-abo/swiper/pull/1059

  (setq ivy-initial-inputs-alist nil)
  ;; This normally maps from a function (?) to the string to populate the prompt with, mostly
  ;; just "^".  Maybe I'm doing something wrong, but I often don't know the exact function name
  ;; and search by keywords.  Turn all defaults off.

  ;;  (setq ivy-re-builders-alist
  ;;        ;; The Spacemacs default is ivy--regex-ignore-order but it doesn't work at all - it
  ;;        ;; separates by space and uses "or" so "a b" matches any line with "a" or "b".  I need
  ;;        ;; "and".
  ;;        '((t . ivy--regex-plus)))

  (setq avy-style 'words)
  ;; Instead of making up non-sense letters, use words as jump targets.  This is a huge
  ;; improvement if you are a touch typist.

  ;; I spent some time looking at the words and found 5 two letter words that were excluded for
  ;; a single three letter word.  In other words, I could take one letter off of 5 three letter
  ;; words.
  ;;
  ;; I also noticed the 'q' and 'x' weren't used, so we can use it as a single letter "word".
  ;; Remember, the goal isn't actually "words" but easy to type entries.  This is replacing
  ;; nonsense combos like "jlk".  Also, 'y' was only used for a single word, so we might as
  ;; well just use the latter.
  ;;
  ;; You can also try digits, but they are farther from the home row and I'm more likely
  ;; to miss.  (Though it might be good typing practice.)
  ;;
  ;; Common puncutation like ;,. seems to work pretty well.
  ;;
  ;; I find I rarely jump the first or last on a page.  You might reorder your initial items to
  ;; the first couple are not your best.
  (setq avy-words
        '(
        ;;  "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" ":"
        ";" "," "." "/"
        "q" "x" "y"
        "ad" "am" "at" "by" "if" "is" "it" "my" "of" "on" "ox" "up" "use"
        "ace" "act" "age" "ago" "aim" "air" "ale" "all" "and" "ant" "any"
        "ape" "apt" "arc" "are" "arm" "art" "ash" "awe" "axe" "bad" "bag"
        "ban" "bar" "bat" "bay" "bed" "bee" "beg" "bet" "bid" "big" "bit" "bob"
        "bot" "bow" "box" "boy" "but" "cab" "can" "cap" "car" "cat" "cog" "cop"
        "cow" "cry" "cup" "cut" "day" "dew" "did" "die" "dig" "dim" "dip" "dog"
        "dot" "dry" "dub" "dug" "dye" "ear" "eat" "eel" "egg" "ego" "elf" "eve"
        "eye" "fan" "far" "fat" "fax" "fee" "few" "fin" "fit" "fix" "flu" "fly"
        "foe" "fog" "for" "fox" "fry" "fun" "fur" "gag" "gap" "gas" "gel" "gem"
        "get" "gig" "gin" "gnu" "god" "got" "gum" "gun" "gut" "guy" "gym" "had"
        "hag" "ham" "has" "hat" "her" "hid" "him" "hip" "his" "hit" "hop" "hot"
        "how" "hub" "hue" "hug" "hut" "ice" "icy" "imp" "ink" "inn" "ion" "ire"
        "ivy" "jab" "jam" "jar" "jaw" "jet" "job" "jog" "joy" "key" "kid" "kit"
        "lag" "lap" "lay" "let" "lid" "lie" "lip" "lit" "lob" "log" "lot" "low"
        "mad" "man" "map" "mat" "may" "men" "met" "mix" "mob" "mop" "mud" "mug"
        "nag" "nap" "new" "nil" "nod" "nor" "not" "now" "nun" "oak" "odd"
        "oil" "old" "orb" "ore" "ork" "our" "out" "owl" "own" "pad" "pan"
        "par" "pat" "paw" "pay" "pea" "pen" "pet" "pig" "pin" "pit" "pod" "pot"
        "pry" "pub" "pun" "put" "rag" "ram" "ran" "rat" "raw" "ray" "red" "rib"
        "rim" "rip" "rob" "rod" "rot" "row" "rub" "rug" "rum" "run" "sad" "sat"
        "saw" "say" "sea" "see" "sew" "she" "shy" "sin" "sip" "sit" "six" "ski"
        "sky" "sly" "sob" "son" "soy" "spy" "sum" "sun" "tab" "tad" "tag" "tan"
        "tap" "tar" "tax" "tea" "the" "tie" "tin" "tip" "toe" "ton" "too" "top"
        "toy" "try" "tub" "two" "urn" "van" "war" "was" "wax" "way" "web"
        "wed" "wet" "who" "why" "wig" "win" "wit" "woe" "won" "wry" "zap" "zip"
        "zoo"))


  ;; Also consider (avy-linenum-mode).  It displays letters for each line and makes it super
  ;; quick to jump to a given line.  I'm liking avy-style words so much that I'm going to wait
  ;; on this to see if I really need it.
  ;;
  ;; Update: It does not work in org-mode.

  ;; Here's something fun I learned.  counsel-jump-in-buffer uses imenu which apparently does
  ;; not rebuild the list of things to jump to even if the file changes.  First, there is a
  ;; variable that causes a rescan which defaults to nil (off).  Second, even if you turn it
  ;; on, there is a maximum file size that overrides that and it is very small: 60K bytes.
  ;;
  ;;  (setq imenu-auto-rescan t
  ;;        imenu-auto-rescan-maxout (* 4 1024 1024))
  ;;
  ;; Building an imenu index in Javascript files is *terrible*.  It hangs all the time, so I'd
  ;; rather have to manually rescan.

  ;;; Smerge
  (use-package smerge-mode
    :init
    (progn
      (defun local/enable-smerge-maybe ()
        "Auto-enable `smerge-mode' when merge conflict is detected."
        (save-excursion
          (goto-char (point-min))
          (when (re-search-forward "^<<<<<<< " nil :noerror)
            (smerge-mode 1))))
      (add-hook 'find-file-hook #'local/enable-smerge-maybe :append)))

  (spacemacs/set-leader-keys "gr" 'hydra-smerge/body)

  ;; Add a new option "B" to copy both A and B to the C buffer.  Use '~' to swap A and B if you
  ;; need the results in a different order.
  ;;
  ;; https://stackoverflow.com/questions/9656311/conflict-resolution-with-emacs-ediff-how-can-i-take-the-changes-of-both-version/29757750#29757750

  (with-eval-after-load 'ediff
    (defun ediff-copy-both-to-C ()
      (interactive)
      (ediff-copy-diff ediff-current-difference nil 'C nil
                       (concat
                        (ediff-get-region-contents ediff-current-difference 'A ediff-control-buffer)
                        (ediff-get-region-contents ediff-current-difference 'B ediff-control-buffer))))
    (defun add-d-to-ediff-mode-map () (define-key ediff-mode-map "B" 'ediff-copy-both-to-C))
    (add-hook 'ediff-keymap-setup-hook 'add-d-to-ediff-mode-map)
    )

  (setq-default fill-column 95)

  (setq sentence-end-double-space t)

  ;; Load our own custom functions early in case we want to bind keys to them.
  (load-file "~/.spacemacs.d/custom-functions.el")

  ;; Clean up buffers at midnight.  Emacs is one of those apps you typically leave running for
  ;; days and buffers are not always visible.  Also note that this runs midnight-hook and you
  ;; can add your own things to it.
  (midnight-mode t)

  ;; I don't want the default snippets, so before yasnippet gets loaded, replace
  ;; the path.  (If you do want the defaults, this path is already part of the
  ;; default.)
  (setq yas-snippet-dirs '("~/.spacemacs.d/snippets"))

  ;; Have Tab jump out of parens and similar pairs.  It's always easier to hit tab, but when
  ;; using a 40% keyboard like a Planck it is a lot easier.
  ;;
  ;; If you are having trouble with this, it will be because modes are monkeying with bindings.

  (defun smart-tab-jump-out-or-indent (&optional arg)
    "Smart tab behavior: jump over a close brackets, etc. but indent otherwise.
This also jumps over a semicolon.  To allow you to indent strings, it will not
jump over a quote beginning a string."
    (interactive "P")
    ;; Am I missing something.  Is there really no way to include ] inside a character
    ;; alternative?  You can't use a backslash to escape them in Emacs.  I'll look for
    ;; the char by itself and then an alternative.
    ;;
    ;; The first regexp looks for ] or one of )}>;`
    ;;
    ;; The second maches single or double quotes, but only if we are inside of a string.
    ;; See the documentation of parse-partial-sexp for documentation on the data structure
    ;; returned by syntax-ppss.  If the 3rd element is non-nil, we are inside a string.
    (if (or (looking-at-p "\\(]\\|[)}>;`']\\)")
            (and (looking-at-p "['\"]")
                 (nth 3 (syntax-ppss))))
        (forward-char 1)
      (indent-for-tab-command arg)))

  (global-set-key [remap indent-for-tab-command] 'smart-tab-jump-out-or-indent)

  ;; Sometimes I really do want to indent at a closing pair, so I'll bind shift-tab to it.  I
  ;; can't bind shift-tab to indent-for-tab-command since the remap will override that too. I
  ;; have to write my own function that does nothing except call the original.

  (defun local/indent-tab (&optional arg)
    (interactive "P")
    (indent-for-tab-command arg))
  (global-set-key (kbd "<backtab>") 'local/indent-tab)

  ;; I don't use make files and if I did I'd just set the compile command to
  ;; make anyway. Set the more convenient key to be compile.
  (spacemacs/set-leader-keys "cc" 'compile)

  ;; I use rg a lot, so I'll make it easy to get to
  (global-set-key (kbd "M-s r") 'rg)

  ;; When correcting typos, I often want to jump to a letter on the same line.  Usually f and F
  ;; are good enough, but this is a pretty useful function when a letter is very common.
  (spacemacs/set-leader-keys "jc" 'avy-goto-char-in-line)

  ;; The default "beginnning of line" commands are '0' for absolute beginning and '^' for first
  ;; non-whitespace character.  First character is the most useful but '^' is not nearly as
  ;; easy to hit as '0'.  Instead of swapping them, bind '0' to a function that first goes to
  ;; the first character, but goes to the absolute beginning if pressed again.
  (define-key evil-normal-state-map (kbd "0") 'smarter-move-beginning-of-line)

  ;; I love the convenience of SPC-tab for switching between the last two buffers, but the
  ;; default implementation won't jump back to a magit status buffer.  It uses other-buffer
  ;; (the function responsible for finding the "next" buffer) which calls buffer-predicate to
  ;; filter out "uninteresting" buffers - typically those with a star in the name I believe.
  ;; (I haven't seen the source; just going by behavior.)  spacemacs is, as usual, a step ahead
  ;; of me and has added a list of regular expressions to augment this.
  (setq spacemacs-useful-buffers-regexp '("\\*scratch\\*" "magit:" "\\*SQL\\*" "\\*rg\\*"))

  ;; Vi modal editing is nice, but when in insert mode, you should be able to
  ;; move around just a bit.  Moving by characters and words is supported, but
  ;; we'll add some other standard ones that used by readline (Bash, etc.).
  ;;
  ;; You can see keybindings in insert mode using which-key-show-top-level (M-m h k).
  (define-key evil-insert-state-map (kbd "C-a") 'move-beginning-of-line) ;; was 'evil-paste-last-insertion
  (define-key evil-insert-state-map (kbd "C-e") 'end-of-line)    ;; was 'evil-copy-from-below
  (define-key evil-insert-state-map (kbd "C-j") 'next-line)
  (define-key evil-insert-state-map (kbd "C-k") 'previous-line)
  (define-key evil-insert-state-map (kbd "C-d") 'evil-delete-char)
  (define-key evil-insert-state-map (kbd "C-f") 'evil-forward-char)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-backward-char)
  (define-key evil-insert-state-map (kbd "C-l") 'evil-forward-char)

  (define-key evil-insert-state-map (kbd "C-t") 'transpose-chars)
  (define-key evil-normal-state-map (kbd "C-t") 'transpose-chars)

  ;; evil-update-insert-state-bindings seems to be overriding our customizations, even though
  ;; it is not supposed to.  I wasn't able to locate where it was called from, so for now we'll
  ;; use advice to put our bindings back after it overrides them.

  (defun local/update-bindings()
    (define-key evil-insert-state-map (kbd "C-k") 'previous-line)
    (define-key evil-insert-state-map (kbd "C-t") 'transpose-chars)
    )

  (advice-add 'local/update-bindings :after #'evil-update-insert-state-bindings)

  ;; I often find myself at the end of the line with a bunch of closed quotes and parens after
  ;; the cursor and I want to skip them all and go to the next line.
  (define-key evil-normal-state-map (kbd "<C-return>") 'evil-open-below)
  (define-key evil-insert-state-map (kbd "<C-return>") 'evil-open-below)

  ;; I'm not sure if this is a good idea, but evil-mode doesn't use Cmd-c and Cmd-v so I'm
  ;; going to set them to the Mac-compatible copy and paste.
  (global-set-key (kbd "M-c") 'kill-ring-save)
  (global-set-key (kbd "M-v") 'evil-paste-before)

  ;; Somehow this is getting overridden by clean-aindent--bsunindent and I
  ;; can't figure out how to get rid of it.
  (global-set-key (kbd "<M-backspace>") 'backward-kill-word)

  (global-set-key (kbd "M-\\") 'cycle-spacing)

  ;; I'm not sure why, but recent Spacemacs changed SPC gff from magit-find-file, which is very
  ;; useful, to counsel-git-file which does the same thing as SPC pf to load files from the
  ;; project.  The magit function is now just a capital f, but I am very used to the old
  ;; binding and the update doesn't make sense.

  (spacemacs/set-leader-keys "gff" 'magit-find-file)

  (spacemacs/set-leader-keys "od" 'insert-date)
  ;; This is a custom function to insert the date into the buffer in YYYY-MM-DD format.
  ;;
  ;;  (global-set-key (kbd "C-s") 'swiper-isearch)

  (use-package poporg
    ;; This allows editing of comments and Python docstrings in a temporary org-mode buffer.
    ;;
    ;; Use C-c ' in a comment or docstring to begin editing.  I've chosen this to match org's
    ;; key for editing source blocks.  Like many other special buffers in Spacemacs, such as
    ;; magit commit buffers, use ",c" to commit and ",k" to abort.
    ;;
    ;; One problem is whitespace.  When saving, the default behavior is to remove whitespace on
    ;; blank lines when adding back the prefix, which is great, but it also removes the leading
    ;; blank line from Python triple-quoted docstrings.  I want the first line of the
    ;; documentation to be below the quotes.
    ;;
    ;; For now, I'm going to disable poporg-delete-trailing-whitespace and manually clean items
    ;; with whitespace-cleanup.  I've configured it to not remove blank lines.  Unfortunately
    ;; this leaves whitespace on blank lines if there is a comment prefix.  I've added a file
    ;; save hook to cleanup whitespace, so all I have to do is save the code buffer to fix it
    ;; so it will do for now.  It would be ideal if I could clean the whitespace from the
    ;; region in the poporg exit hook, but I'm not sure how to get it.  (The hook runs after
    ;; the text has been put back into the code buffer.)
    :commands (poporg-dwim)
    :bind (("C-c '" . poporg-dwim)
           :map poporg-mode-map
           ([remap save-buffer] . poporg-update)
           ;; poporg has remapped save-buffer to exit, but that really annoys me.  I have a
           ;; habit of saving the buffer before I'm finished.  I'm mapping it to update without
           ;; exit like an org source code block.
           )
    :config
    (spacemacs/set-leader-keys-for-minor-mode 'poporg-mode "c" #'poporg-edit-exit)
    (spacemacs/set-leader-keys-for-minor-mode 'poporg-mode "k" #'kill-current-buffer)
    (setq poporg-delete-trailing-whitespace nil)
    (defun local/poporg-cleanup(with-save)
      (whitespace-cleanup))
    (advice-add 'poporg-update :before #'local/poporg-cleanup))

  ;; Normally search commands like 'f' and 't' only work on the current line.  I don't see why
  ;; I wouldn't want them to go to the next line.  Trying this out.  There might be a reason it
  ;; isn't a default setting.
  (setq evil-cross-lines t)

  ;; This has been a pain in my side for a long time.  I often want to delete items *following*
  ;; the text I'm working on, but without this I usually end up *on* the final character which
  ;; I want to keep.  Not moving the cursor back helps too.
  (setq evil-move-beyond-eol t)

  (setq evil-move-cursor-back nil)

  ;; This is a standard vim scrolling key, but evil disables by default because C-u is so
  ;; common with emacs users for the prefix argument.  (Use SPC u in Spacemacs.)
  (setq evil-want-C-u-scroll t)

  ;; Don't flash the new state (e.g. "INSERT") in the echo area when changing the state.
  ;; Spacemacs already recolors a state area and this seems noisy.
  (setq evil-echo-state nil)

  (global-git-commit-mode t)

  (setq create-lockfiles nil)

  ;; Some modes make sense to start in insert-mode.
  (evil-set-initial-state 'git-commit-mode 'insert)
  (evil-set-initial-state 'Custom-mode 'normal)
  (evil-set-initial-state 'compilation-mode 'normal)

  (set-fill-column 95)
  (auto-fill-mode t)

  ;; I don't need visual word wrapping very often and it is very easy to toggle on when
  ;; needed, so default to on (which means do not wrap).  To toggle: SPC t l
  ;;
  ;; I tried using (spacemacs/toggle-truncate-lines-on) but it doesn't work.  First, it may
  ;; have a bug since it calls (toggle-truncate-lines) with no argument.
  ;;
  ;; All of the toggling eventually sets the variable `truncate-lines' which is a *buffer
  ;; local* variable, so use setq-default.
  (setq-default truncate-lines t)

  ;; When I split a window, it is because I am trying to find something somewhere else for
  ;; reference, not because I'm trying to see more around where I'm working.  I just don't get
  ;; the point of follow-mode at the moment.
  (turn-off-follow-mode)

  ;; dumbjump is a simple go-to-definition implementation that uses a simple search.  If rg is
  ;; installed it is amazingly fast.  No need for tags files (though I wish it could guess
  ;; among multiple functions with the same name based on whether it is in the current file and
  ;; based on number parameters.)
  ;;
  ;; Since it is "dumb", it is at the end of the jump handler list.  However, the only other
  ;; jump handlers is tags based which I don't want.  This removes it.
  (setq spacemacs-default-jump-handlers '(dumb-jump-go))
        ;;  (remove 'evil-goto-definition spacemacs-default-jump-handlers))

  (setq spacemacs-jump-handlers-sql-mode '(dumb-jump-go))
  ;; For some reason this is empty and doesn't use the default ones.  Maybe it is initialized
  ;; before we get to the lines above?

  ;; Spacemacs autoindents when pasting, but sql-mode doesn't have decent indenting so a nicely
  ;; formatted large query gets "flattened".  https://github.com/syl20bnr/spacemacs/issues/4219
  (add-to-list 'spacemacs-indent-sensitive-modes 'sql-mode)

  (defun local-sql-hook()
      (turn-on-auto-fill))

  (defun local-sql-interactive-hook()
    ;; When closing emacs, don't ask if we want to stop just because of a SQL buffer.
    (setq sql-display-sqli-buffer-function #'display-buffer))

  (add-hook 'sql-interactive-mode-hook 'local-sql-interactive-hook)
  (add-hook 'sql-mode-hook 'local-sql-hook)

  ;; Make a quick-key for getting to the *SQL* buffer.  I've defined a function since that is
  ;; what is displayed by which-key.

  (defun open-sql()
    (interactive)
    (sql-product-interactive))
  (spacemacs/set-leader-keys "os" 'open-sql)

  ;; Accidentally opening a minified JS file, which is 1 long line, will kill Emacs on Windows.
  ;; This mode switches to fundamental mode.  I accidentally do this because I'm not used to
  ;; the evilified dired commands.  They aren't very good.
  ;;
  ;; This is included in emacs 27 so you should remove the filename when upgrading.
  ;;
  ;; Use C-c C-c in so-long mode to revert back to the normal mode for the file.  For example,
  ;; if loading a very large single-line JSON export file which kicks into so-long mode, you
  ;; could:
  ;;
  ;; 1. Turn off read-only using C-x C-q
  ;; 2. Format to multiple lines by running M-x json-pretty-print-buffer
  ;; 3. Switch to JSON mode using C-c C-c

  (require 'so-long "~/.spacemacs.d/so-long.el")
  (global-so-long-mode 1)
  (setq so-long-threshold 1000)
  (mapc (apply-partially #'add-to-list 'so-long-target-modes)
        '(json-mode))

  ;; This also helps with the speed problem apparently.  I don't use bidi, so turning this off
  ;; is one less thing to check during file opening.
  (setq-default bidi-display-reordering nil)


  (use-package dired
    :config
    (setq-default dired-omit-files-p t)
    (setq-default dired-omit-verbose nil)
    (evilified-state-evilify dired-mode dired-mode-map
      "gg" 'local/dired-first-file
      "G" 'local/dired-last-file)
    (setq dired-listing-switches "-AGFhlv"
          dired-recursive-copies 'always
          dired-recursive-deletes 'always
          dired-deletion-confirmer 'y-or-n-p)
    ;; Is there a use-package way to define SPC-mo?
    (spacemacs/set-leader-keys-for-major-mode 'dired-mode "o" 'dired-omit-mode)

    :hook ((dired-mode . dired-hide-details-mode)
           (dired-mode . hl-line-mode))
    )

  (use-package dired-x
    :after dired
    :config
    (setq dired-clean-up-buffers-too t)
    (setq dired-bind-man nil)
    (setq dired-bind-info nil))

  (use-package pcre2el
    ;; This causes all (most?) regexp inputs to accept PCRE (normal) regular expressions instead
    ;; of the older emacs style.
    :config
    (pcre-mode 1)
    (diminish 'pcre-mode)                 ; "PCRE"
    )

  ;; Eliminate some text for minor modes that are always on.

  ;; The next release will already have this set.
  (spacemacs/set-leader-keys "fb" 'counsel-bookmark)

  ;; Spacemacs highlights spaces on the current line while you are typing if you somehow got a
  ;; space in front of the cursor.  This drives me crazy.  Since trailing whitespace is removed
  ;; when I save, I don't need this.
  ;;  (setq-default spacemacs-show-trailing-whitespace nil)

  (load-file "~/.spacemacs.d/evil-textobj-func.el")
  ;; Provide a custom 'if' and 'af' vi text object for the current function (e.g. use "vif" to
  ;; highlight the current fucntion.)

  ;; TODO: Make this demand load.
  (load-file "~/.spacemacs.d/phonetic.el")
  ;; Provides phoneticize-region - I really need to work on my ability to come up with these
  ;; on the fly.

  ;; Set Python docstrings so the text is not on the same line as the triple quotes.
  (setq python-fill-docstring-style (quote django)
        python-honour-comment-indentation nil)

  ;; On Windows, anaconda causes horrible stuttering and delays.  I don't really use it much
  ;; and would disable it completely if I knew how.  For now, set a completion delay.
  (remove-hook 'python-mode-hook 'spacemacs//init-eldoc-python-mode)
  (remove-hook 'python-mode-hook 'anaconda-mode)

  (defun local/python-hook()
    (setq-local comment-auto-fill-only-comments t)
    (turn-on-auto-fill)
    ;; (setq electric-indent-inhibit t)

    (setq spacemacs-jump-handlers-python-mode '(dumb-jump-go))
    ;; Heck, dumb-jump does a better job than anaconda which is the default.
    ;;
    ;; I tried setting this globally, but it is overridden and anaconda is added, which sucks.
    ;; I don't know if it is configured later and I only need to do this once or if it is being
    ;; configured in a hook somewhere.

    ;;  (color-identifiers-mode 1)

    (anaconda-mode nil)
    (turn-off-anaconda-eldoc-mode)

    ;; I use RST and SQL in triple quoted strings so I've customized indentation in them.
    (define-key python-mode-map (kbd "RET") 'local/python-newline-and-indent)
    (setq indent-line-function 'local/python-indent)

    (abbrev-mode 1)
    ;; ;; Python mode appends "(class)" and "(def)" to everything which looks crappy.
    ;; (setq python-imenu-format-item-label-function (lambda(type name) name))
    (setenv "LANG" "en_US.UTF8"))
  (add-hook 'python-mode-hook #'local/python-hook)


  (when (eq system-type 'darwin)
    (setq ispell-program-name "aspell")
    ;; http://blacka.com/david/2010/01/17/switching-to-cocoa-emacs

    (setq locate-command "mdfind")
    ;; Use M-x locate to run a Spotlight search for a file directly in emacs.
    ;; This is ideal for finding things like configuration files.

    (setq auth-sources '(macos-keychain-internet))
    ;; Allow auth-sources to look in the Keychain for passwords instead of using
    ;; plaintext netrc or authinfo files.

    (spacemacs/set-leader-keys (kbd "f O") 'reveal-in-osx-finder)

    (setq trash-directory "~/.Trash")
    ;; Emacs seems to default to using the FreeDesktop ~/local/share/Trash directory when
    ;; deleting files!  I had a ton of deleted files and it interfered with code that was
    ;; counting symlinks.
    ;;
    ;; Note that whether it does so at all is controlled by delete-by-moving-to-trash which
    ;; Spacemacs (I think) sets to to t.
    )

  (with-eval-after-load 'web-mode
    (setq web-mode-engines-alist '(("ctemplate" . "\\.html\\'")
                                   ("django" . "\\.jinja\\'")))
    )


  (spacemacs/declare-prefix-for-mode 'web-mode "ma" "attrs")
  (spacemacs/declare-prefix-for-mode 'web-mode "me" "elements")
  (spacemacs/set-leader-keys-for-major-mode 'web-mode
    "as" 'web-mode-attribute-select
    "ak" 'web-mode-attribute-kill
    "at" 'web-mode-attribute-transpose
    "es" 'web-mode-element-select
    "ea" 'web-mode-element-content-select
    "ek" 'web-mode-element-kill
    "er" 'web-mode-element-rename
    "ev" 'web-mode-element-vanish
    "ew" 'web-mode-element-wrap
    "ec" 'web-mode-element-clone
    )

  ;; I use GitHub flavored markdown by default since I'm usually documenting code and don't
  ;; want it to italicize partial words (a_b_c).
  (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))

  (defun local-markdown-hook()
    (outline-minor-mode 1)
    ;; (outline-hide-sublevels 1)
    ;;  (outline-hide-body)
    (turn-on-auto-fill))

  (with-eval-after-load 'markdown-mode
    (setq markdown-asymmetric-header t  ; header has hashes on left only
          markdown-gfm-uppercase-checkbox nil
          markdown-edit-code-block-default-mode 'sql-mode
          ;; 99% of the code I use in docs is SQL.
          )
    (add-hook 'markdown-mode-hook 'local-markdown-hook)
    )

  (spacemacs/set-leader-keys-for-major-mode 'gfm-mode
    "hs" 'markdown-mark-subtree)


  (setq flycheck-check-syntax-automatically '(save mode-enable)
        flycheck-checker-error-threshold 1000)
  ;; The default is to check after newline.  Now only check on save.

  (with-eval-after-load 'flycheck
    (progn
      ;; Unfortunately, flycheck has a specific check for web-mode to see if the buffer filename
      ;; matches web-mode's *default* extensions, which for Handlebars is:
      ;;
      ;; - base.hb.html
      ;; - base.hbs
      ;;
      ;; I've set web-mode to always use Handlebars with HTML files so I need to disable this
      ;; check.  The check itself is the `flycheck-predicate' property on the 'handlebars checker.
      ;; We'll simply delete it.

      (put 'handlebars 'flycheck-predicate nil)

      ;; I want to run both flake8 and pylint on macOS.  (My Windows box is *way* too slow for this.)
      (when (eq system-type 'darwin)
        (flycheck-add-next-checker 'python-flake8 'python-pylint))
    ))

  (setq js2-basic-offset 2
        js-indent-level 2)

  (defun local-js2-hook()
    ;; This is unbelievable but js2 redefines the next-error-function away from flycheck /
    ;; flymake with no options to turn it off.

    ;;  (color-identifiers-mode 1)
    ;; Autofill does not work in js2 right now.  When it wraps, it does not indent the new
    ;; line.  This is worse than not wrapping at all.
    ;;  (turn-on-auto-fill)
    (setq next-error-function 'flycheck-next-error
          previous-error-function 'flycheck-previous-error))

  (add-hook 'js2-mode-hook 'local-js2-hook)

  (with-eval-after-load 'projectile
    (setq projectile-enable-caching t
          projectile-switch-project-action (if (not local/this-box-is-slow) 'projectile-vc 'projectile-dired)
          projectile-use-git-grep t
          projectile-globally-ignored-file-suffixes (quote (".py[oc]"))
          ;; Use fd instead of find (though I'm not sure when this gets used).
          projectile-generic-command "fd . -0"

          ;; This is already the default on macOS and Linux.  On Windows, I have the
          ;; necessarily utilities like git and fd so it should work.  The native indexing is
          ;; written in elisp and is far too slow on Windows.
          projectile-indexing-method 'alien)

    (setq projectile-globally-ignored-files
          (append projectile-globally-ignored-files
                  '(".DS_Store" ".gitignore" "*.pyo" "*.dll" "*.pdf" "*.exe" "*.pyc" "*.elc")))
    (setq completion-ignored-extensions
          (append completion-ignored-extensions '(".pyc" ".pyo")))

    ;; This seems like something we need a function for.  All 3 of these are the same, it is
    ;; just from source --> other files.  I can't imagine them not always being the same.
    (add-to-list 'projectile-other-file-alist '("html" "js" "py"))
    (add-to-list 'projectile-other-file-alist '("hbs" "js" "py"))
    (add-to-list 'projectile-other-file-alist '("js" "py" "html" "hbs"))
    )

  (with-eval-after-load 'counsel-projectile
    (setq counsel-projectile-switch-project-action
          (if (not local/this-box-is-slow) 'counsel-projectile-switch-project-action-vc 'counsel-projectile-switch-project-action-dired))
    )

  (use-package table
    :config
    ;; Decrease the time between typing and table updating.  Oh my God it is hard to put up
    ;; with.
    (setq table-time-before-reformat 0.05
          table-time-before-update 0.05)
    )

  (use-package rst
    :init
    (add-hook 'rst-mode-hook
              (lambda()
                ;; Turn off ligatures in rst mode - a line of equals turns into a crazy looking
                ;; list of triple-equals characters or worse: ====
                (auto-composition-mode 0))))


  (use-package org
    :config
    ;; This is the old value
    ;;  (ol-w3m ol-bbdb ol-bibtex ol-docview ol-gnus ol-info ol-irc ol-mhe ol-rmail ol-eww)
    (setq org-modules '(org-drill))
    ;; On Windows, the circles I like are rendering larger than a normal character so everything
    ;; is misaligned.  I'll use some other symbols I found on the internet for Windows.
    ;;
    ;; (Is this the "lispy" way of doing this?  I love that everything in lisp is an expression
    ;;  so 'if' statements return a value, but I would like to learn common lisp idioms.)
    (setq org-superstar-headline-bullets-list
          (if (eq system-type 'windows-nt)
              '("◆" "■" "▲" "▶")
            '("●" "○" "◎"))

          org-todo-keywords
          '((sequence "TODO" "URGENT" "|" "DONE"))

          org-hide-emphasis-markers t

          org-confirm-babel-evaluate (lambda (lang body)
                                       (not (string= lang "sql-mode"))))

    (add-to-list 'org-mode-hook
                 (lambda ()
                   (when (eq system-type 'darwin)
                     (require 'ob-sql-mode))
                   (org-num-mode)
                   (auto-fill-mode t)))

    )

  ;;  (setq org-superstar-headline-bullets-list '("①" "②" "③" "④" "⑤" "⑥" "⑦" "⑧" "⑨"))
  ;;  (setq org-superstar-headline-bullets-list '("一" "二" "三" "四" "五" "六" "七" "八" "九"))
  ;;  (setq org-superstar-headline-bullets-list '("壱" "弐" "四" "四" "五" "六" "七" "八" "九"))

  ;; These words are highlighted.  The top two are used in my org mode docs.
  (setq hl-todo-keyword-faces
        '(("TODO" . "#cc9393")
          ("URGENT" . "#cc9393")
          ("NOTE" . "#d0bf8f")
          ("HACK" . "#d0bf8f")
          ("REVIEW" . "#d0bf8f")))

  ;; The spacemacs default keybindings set `SPC p a' to switch to a test file but I find
  ;; projectile's "alternate" file much handier.  Also, the default projectile key binding for
  ;; this was "a".
  (spacemacs/set-leader-keys "p a" 'projectile-find-other-file)

  (autoload 'c++-mode "cc-mode" "C++ Editing Mode" t)
  (autoload 'c-mode   "cc-mode" "C Editing Mode" t)

  ;; Force .h files into C++ mode; usually they are assigned to C mode.
  (setq auto-mode-alist (cons '("\\.h$" . c++-mode) auto-mode-alist))

  (defconst local/c-style
    '((c-tab-always-indent        . t)
      (c-comment-only-line-offset . 4)
      (c-hanging-braces-alist     . ((substatement-open after)
                                     (brace-list-open)))
      (c-hanging-colons-alist     . ((member-init-intro before)
                                     (inher-intro)
                                     (case-label after)
                                     (label after)
                                     (access-label after)))
      (c-cleanup-list             . (scope-operator
                                     empty-defun-braces
                                     defun-close-semi))
      (c-offsets-alist            . ((arglist-close . c-lineup-arglist)
                                     (substatement-open . 0)
                                     (case-label        . 0)
                                     (block-open        . 0)
                                     (knr-argdecl-intro . -)
                                     (inline-open       . 0)
                                     (comment-intro     . 0)
                                     ))
      (c-echo-syntactic-information-p . t)
      )
    "My C Programming Style")

  (defun local/c-mode-common-hook ()
    ;; make the <enter> key use the "smart enter" function
    (define-key c-mode-base-map "\C-m" 'newline-and-indent)

    ;; I finally got tired of hungry delete, but here it is for those that like it.
    ;; (c-toggle-auto-hungry-state 1)
    (c-toggle-auto-newline 1)
    (c-toggle-auto-newline nil)

    (c-add-style "PERSONAL" local/c-style t)
    ;; (setq compile-command "nmake /NOLOGO  ")
    (setq c-block-comment-prefix "* ")
    ;; (setq c-hanging-comment-ender-p nil)

    ;; override C-M-h to narrow-sexp.  Mark-sexp doesn't work for Java code, so we
    ;; grabbed its keystroke.
    ;;(global-set-key "\C-\M-h" 'narrow-sexp)

    ;; Spacemacs has multiple bugs open about not setting jump handlers properly.  This is a
    ;;  buffer-local setting.
    (setq spacemacs-jump-handlers '(dumb-jump-go))

    (turn-on-auto-fill)
    ;; (setq auto-fill-function c-do-auto-fill)

    ;; override the default cleanup list to remove emtpy-defun-braces
    (setq c-cleanup-list '(scope-operator defun-close-semi))

    (setq comment-start "// ")
    (setq comment-end "")
    )

  (add-hook 'c-mode-common-hook 'local/c-mode-common-hook)

  ;; Calling undo undoes a *ton* of stuff sometimes.  You want to just delete the last word and
  ;; it deletes the last 30 minutes of work.  I don't remember this happening in vanilla emacs
  ;; so I assumed it was related to Spacemacs or evil, but the evil issue 594 says that Emacs
  ;; changed in 25.1: https://github.com/emacs-evil/evil/issues/594

  (setq evil-want-fine-undo t)

  ;; https://emacs.stackexchange.com/questions/28736/emacs-pointcursor-movement-lag/28746
  ;;
  ;; Apparently cursor down is very slow?  Not noticing a huge difference but will try it.
  (setq auto-window-vscroll nil)

  (when (eq system-type 'darwin)
        (pixel-scroll-mode 1))

  ;;  ;; org-mode and agenda.  I'm only using this on my Mac.
  ;;  (when (eq system-type 'darwin)

  ;;    ;; I'm keeping everything in ~/todo.org
  ;;    (setq org-agenda-files (quote ("~/todo.org")))

  ;;    ;; On startup, scan the todo file for appointments for today...
  ;;    (org-agenda-to-appt)

  ;;    ;; ... and if Emacs is still running over night, do it again for tomorrow ...
  ;;    (run-at-time "12:05am" (* 24 3600) 'org-agenda-to-appt)

  ;;    ;; ... and finally any time I save todo.org
  ;;    (add-hook 'after-save-hook
  ;;              '(lambda ()
  ;;                 (if (string= (buffer-file-name) (concat (getenv "HOME") "/todo.org"))
  ;;                     (org-agenda-to-appt))))

  ;;    ;; And show today's agenda at startup.
  ;;    (org-agenda nil "a"))

  ;; Stuff that only applies to the current computer is stored in
  ;; local-config.el and is not checked in.  (It is in .gitignore.)  Since it is
  ;; not checked in, this is also where you'd store confidential items.

  ;; Add 'go' operation which replaces the text object or motion without modifying the
  ;; clipboard.
  ;;
  ;; https://www.reddit.com/r/spacemacs/comments/cl2q0f/how_have_you_tweaked_spacemacs_or_emacs_using/

  (evil-define-operator local/evil-replace-with-kill-ring (beg end)
    "Replace with killring action."
    :move-point nil
    (interactive "<r>")
    (save-excursion
      (delete-region beg end)
      (goto-char beg)
      (call-interactively 'evil-paste-before 1)))

  (define-key evil-normal-state-map "go" 'local/evil-replace-with-kill-ring)


  (let ((local "~/.spacemacs.d/local-config.el"))
    (if (file-exists-p local)
        (load-file local)))
 )

(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(compilation-ask-about-save nil)
 '(compilation-read-command nil)
 '(counsel-find-file-ignore-regexp "\\.pyc$")
 '(custom-safe-themes
   (quote
    ("89187377b319f243c2173f94adc4d1100c3527e52e3bb74f30435a97b4518252" "4e753673a37c71b07e3026be75dc6af3efbac5ce335f3707b7d6a110ecb636a3" default)))
 '(evil-magit-want-horizontal-movement t)
 '(evil-search-module (quote evil-search))
 '(evil-want-Y-yank-to-eol t)
 '(fill-column 95)
 '(grep-command "grep -srHIn ")
 '(magit-commit-ask-to-stage (quote stage))
 '(magit-diff-arguments (quote ("--ignore-space-change" "--no-ext-diff")))
 '(magit-log-arguments
   (quote
    ("--color" "-n256" "--graph" "--decorate" "--date=human")))
 '(magit-merge-arguments (quote ("--ff-only")))
 '(magit-revision-show-gravatars nil)
 '(markdown-asymmetric-header t)
 '(package-selected-packages
   (quote
    (poporg string-edit org-drill ansi shut-up git commander noccur color-identifiers-mode ivy-rich modus-vivendi-theme modus-operandi-theme faff-theme which-key-posframe doneburn-theme github-modern-theme speed-type ivy-posframe treemacs-evil org-download ob-sql-mode forge transient lv doom-modeline window-purpose all-the-icons treemacs org-plus-contrib ghub let-alist reveal-in-osx-finder git-gutter-fringe+ git-gutter-fringe fringe-helper git-gutter+ git-gutter diff-hl flycheck-pos-tip pos-tip flycheck visual-fill-column fuzzy helm-themes helm-swoop helm-pydoc helm-projectile helm-mode-manager helm-gitignore helm-flx helm-descbinds helm-css-scss helm-ag flyspell-correct-helm ace-jump-helm-line evil-goggles writeroom-mode focus solarized-theme-theme color-theme-sanityinc-solarized ibuffer-projectile winum python graphviz-dot-mode nginx-mode rainbow-mode bm csv-mode zenburn-theme sublime-themes hc-zenburn-theme darkburn-theme company-web web-completion-data company-tern dash-functional tern company-statistics company-anaconda company auto-yasnippet ac-ispell auto-complete imenu-list selectric-mode web-mode tagedit slim-mode scss-mode sass-mode pug-mode less-css-mode haml-mode emmet-mode sql-indent hide-comnt flyspell-correct-ivy flyspell-correct auto-dictionary smeargle orgit org magit-gitflow gitignore-mode gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link evil-magit magit magit-popup git-commit with-editor mmm-mode markdown-toc markdown-mode gh-md yapfify pyvenv pytest pyenv-mode py-isort pip-requirements live-py-mode hy-mode cython-mode anaconda-mode pythonic web-beautify livid-mode skewer-mode simple-httpd json-mode json-snatcher json-reformat js2-refactor yasnippet multiple-cursors js2-mode js-doc coffee-mode yaml-mode window-numbering which-key wgrep volatile-highlights vi-tilde-fringe uuidgen use-package toc-org spaceline powerline smex restart-emacs request rainbow-delimiters popwin persp-mode pcre2el paradox spinner org-bullets open-junk-file neotree move-text macrostep lorem-ipsum linum-relative link-hint ivy-hydra info+ indent-guide ido-vertical-mode hydra hungry-delete hl-todo highlight-parentheses highlight-numbers parent-mode highlight-indentation help-fns+ helm-make helm helm-core google-translate golden-ratio flx-ido flx fill-column-indicator fancy-battery eyebrowse expand-region exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-surround evil-search-highlight-persist evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-lisp-state smartparens evil-indent-plus evil-iedit-state iedit evil-exchange evil-escape evil-ediff evil-args evil-anzu anzu evil goto-chg undo-tree eval-sexp-fu highlight elisp-slime-nav dumb-jump popup f s diminish define-word counsel-projectile projectile pkg-info epl counsel swiper ivy column-enforce-mode clean-aindent-mode bind-map bind-key auto-highlight-symbol auto-compile packed dash async aggressive-indent adaptive-wrap ace-window ace-link avy quelpa package-build spacemacs-theme)))
 '(paradox-github-token t)
 '(safe-local-variable-values
   (quote
    ((org-confirm-babel-evaluate)
     (eval when
           (fboundp
            (quote rainbow-mode))
           (rainbow-mode 1))
     (eval when
           (require
            (quote rainbow-mode)
            nil t)
           (rainbow-mode 1)))))
 '(web-mode-css-indent-offset 2)
 '(web-mode-enable-auto-quoting nil)
 '(web-mode-markup-indent-offset 2)
 '(which-function-mode t))
)
