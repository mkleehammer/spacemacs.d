;; Custom functions, separated to keep init.el smaller and easier to manage.
;;
;; I'm going to start prefixing functions with "local/".  It is common for prefixes to be
;; people's initials, but something generic makes it easier to copy to your own as-is.

;; The dired mode's keybindings are not very good yet.  This is used to jump to the first and
;; last filename which we bind to gg and G.

(defun local/insert-random-number (len)
  "Insert random number of length LEN"
  (interactive "P")
  (insert (number-to-string (random 999999999))))

(defun local/dired-first-file()
  "Move to the first file."
  (interactive)
  (beginning-of-buffer)
  (dired-next-line 2))

(defun local/dired-last-file()
  "Move to last file."
  (interactive)
  (end-of-buffer)
  (dired-next-line -1))

(defun font-lock-face()
  "Displays the font-lock face at point."
  (interactive)
  ;; Sometimes it isn't obvious what face you are trying to customize.  This tells
  ;; you what face is under the cursor.
  (prin1 (get-text-property (point) 'face)))

(defun insert-date ()
  "Insert date in YYYY-MM-DD format at point."
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))

;; Provide unix-file and windows-file to switch line endings.

(defun unix-file ()
  "Change the current buffer to Latin 1 with Unix line-ends."
  (interactive)
  (set-buffer-file-coding-system 'iso-latin-1-unix t))

(defun windows-file ()
  "Change the current buffer to Latin 1 with Windows line-ends."
  (interactive)
  (set-buffer-file-coding-system 'iso-latin-1-dos t))

(defun hide-windows-eol ()
  "Hide ^M in files containing mixed UNIX and Windows line endings."
  (interactive)
  (setq buffer-display-table (make-display-table))
  (aset buffer-display-table ?\^M []))

;; When I complete a task on a project, I usually kill either everything or
;; everything except the current magit buffer.  I run these with M-x.

(defun kill-all-buffers ()
  "Kill all buffers, asking permission on modified ones."
  (interactive)
  (let ((list (buffer-list)))
    (while list
      (let* ((buffer (car list))
             (name (buffer-name buffer)))
        (and (not (string-equal name ""))
             (kill-buffer buffer)))
      (setq list (cdr list))))
  (cd "~"))

;; kill-other-buffers normally doesn't delete any special buffers (e.g. those
;; with a star like the scratch buffer).  Use C-u before running to kill all
;; other buffers.

(defun kill-other-buffers (&optional arg)
  "Kill all buffers but the current one.
Don't mess with special buffers unless prefix is provided."
  (interactive "P")
  (dolist (buffer (buffer-list))
    (unless (or
             (eql buffer (current-buffer))
             (and (not arg) (not (buffer-file-name buffer))))
      (kill-buffer buffer))))

(defun local/unfill-paragraph (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max)))
    (fill-paragraph nil region)))

;; -----------------------------------------------------------------------------------------------
;; Use python-mode's indentation except when in a triple quoted string.  The default always
;; indents to the beginning of the string, but that is useless when writing docstrings in
;; restructured text or writing large SQL statements.  In those cases, just indent by 4 chars.

(defun local/python-indent()
  (if (eq (car (python-indent-context)) :inside-string)
      (indent-line-to (+ (current-indentation) python-indent-offset))
    (python-indent-line-function)))

(defun local/python-newline-and-indent()
  "newline function for python-mode that indents in strings better"
  (interactive)

  (let* ((ppss (syntax-ppss))
         (isstr (nth 3 ppss))  ; are we in a string?
         (start (nth 8 ppss))) ; start of quotes
    (if isstr
      ;; Calculate the indentation of the nextline based on point before newline.
      (let ((i (local/python-string-indent start)))
        (newline)
        (indent-to i))
    (newline-and-indent))
    )
  )

(defun local/python-string-indent(start)
  "Calculate the proper indentation for local/python-newline-and-indent."

  ;; If on the same line as a line start, there are two possibilities.
  ;;
  ;;   cnxn.execute(
  ;;       """<-- return here
  ;;       <-- should go here
  ;;
  ;;   cnxn.execute("""<-- return here
  ;;       <-- should go here
  ;;
  ;; If the current line is non-empty, indent to the start of it.
  ;;
  ;;   """
  ;;   select<-- return here
  ;;   <-- should go here
  ;;
  ;; On an empty line, assume the previous rules already put us at the correct
  ;; indentation and indent to where the point currently is.
  ;;
  ;; Fortunately (current-indentation) returns point on a blank line so only the
  ;; same-line case needs special handling.

  (let ((line-start  (line-number-at-pos start))
        (point-start (line-number-at-pos (point))))
    (if (= point-start line-start)
        ;; Point is on the same line as the start of quotes.  We already know
        ;; where the quotes start so we just need to know if there is anything
        ;; before.  I don't know of a function that returns the position of the
        ;; first non-empty character (line-beginning-pos returns column 0) so
        ;; we'll use back-to-indentation to move there.
        (save-excursion
          (back-to-indentation)
          (+ (current-indentation)
             (if (/= (point) start)
                 python-indent-offset
               0)))
      (current-indentation)))
  )


;; http://emacsredux.com/blog/2013/05/22/smarter-navigation-to-the-beginning-of-a-line/

(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))


;; pylint-disable-current-warning
;;
;; https://www.reddit.com/r/emacs/comments/g31gtn/generate_comment_to_disable_falsepositive_pylint/
;;
;; Interactive function to add a Python comment on the current line to disable pylint false
;; positive warnings.

(defun pylint-current-error ()
  (first
   (seq-filter
    (lambda (x)
      (eq (flycheck-error-line x)
          (line-number-at-pos)))
    flycheck-current-errors)))

(defun pylint-id-to-symbolic (msg-id)
  (let* ((cmd (format "pylint --help-msg %s" msg-id))
         (msg (shell-command-to-string cmd))
         (start (+ (string-match ":" msg) 1))
         (end (string-match ":" msg start))
         (name (substring msg start end)))
    (car (split-string name " "))))

(defun pylint-disable-warning (msg-id)
  (end-of-line)
  (insert "  ")
  (insert (format "# pylint: disable=%s" msg-id)))

(defun pylint-disable-current-warning ()
  "Add pylint comment to disable warning on current line."
  (interactive)
  (pylint-disable-warning
   (pylint-id-to-symbolic
    (flycheck-error-id
     (pylint-current-error)))))


(defun align-whitespace (start end)
  "Align columns by whitespace"
  (interactive "r")
  (align-regexp start end
                "\\(\\s-*\\)\\s-" 1 0 t))
