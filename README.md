# General

These are my notes while converting from a highly customized Emacs environment
to Spacemacs. Some are here as reminders to me and some are for others starting
the journey.

## Configuration

In standard emacs, everything is in your .emacs.d/init.el file. If I understand
correctly, Spacemacs takes over your .emacs.d and your configuration is moved to
either ~/.spacemacs or ~/.spacemacs.d/init.el. I've chosen the latter so I can
use the directory as a git repository.

Your customizations go into ~/.spacemacs.d/init.el. The documentation recommends
putting things into the user-config function when possible, as this is executed
at the end of configuration. You can use user-init for things that need to be
defined before layers are loaded.

## Key Bindings

I'm not going to repeat the Spacemacs documentation and list all of the key
bindings you can find anywhere, just those that might have been missed or
increase efficiency.

Also note that I've configured Spacemacs to swap Command and Option since all of
the "Alt" keys are the useful ones and the Command key is much easier to hit.
(See my init.el file.)

* `SPC SPC` to get to command list (M-x). Can also be a good way to discover
  assigned keys, which are displayed with the commands.

* `fd` (entered very quickly) is the universal escape, like `C-g` in standard
  Emacs. This eliminates needing to use the escape key and works for getting out
  of almost anything.  (Now you can buy one of the Mac laptops with Touch Bar ;)

* `SPC h d`: the prefix to the various "describe" help functions. If you want to
  know what a key is bound to, for example, use `SPC h d k` then enter the key.

The prefixes `SPC o` and `SPC m o` are reserved for your customizations, so no
package will assign to them.

In insert mode, `ctrl-o` causes next key to be handled in normal state.  This is
handy in buffers that are always in insert mode such as shells or interactive
SQL buffers.  You can stay in insert mode but quickly execute a command line
paste.  (I believe there are better ways, but I have not found them yet.)

Finally, it is worth learning the
[Spacemacs conventions](http://spacemacs.org/doc/CONVENTIONS.html) - these
are keys that apply to a lot of modes.

# VI Info

Spacemacs uses "evil-mode" to emulate vi and Vim, adding some powerful constructs from that
editor family to the extensibility of emacs.  I cannot recommend enough that you read these
articles:

* [The Compositional Nature of Vim](http://ismail.badawi.io/blog/2014/04/23/the-compositional-nature-of-vim/)
* [grokking vi](https://gist.github.com/nifl/1178878)
* [vim text objects - the definitive guide](http://blog.carbonfive.com/2011/10/17/vim-text-objects-the-definitive-guide)

## shift

It seems to be a convention that normal commands operate after the cursor and
pressing shift causes the command to operate before the cursor.  For example `p`
pastes (puts) after the cursor but `P` pastes before.  Similarly, `o` opens a
line after but `O` opens before.

Note that this also extends to search: `/` is a forward search, but `?` searches
backwards.  `?` is what you get when you press shift + '/'.

## Motion and Text Objects

VI lets you apply operations to motion commands or text objects.  For example "w" is a motion
command, so `dw` tells the delete operator to delete from the cursor to the end of the current
word.  Both "aw" and "iw" are *text objects*, so using them applies to the entire word
regardless of where the cursor is in the word.  (Using "a" also selects all whitespace around
the object.)

I **highly** recommend you read this
[VI Text Objects](http://blog.carbonfive.com/2011/10/17/vim-text-objects-the-definitive-guide)
article to really grok how this works.

The list below are the motion commands and text objects I (should) use most often.  Many of
them work as-is and mean "from the cursor" to the object represented.  For example, pressing
`cw` means "change from the cursor to the end of the current word".  However, most can be
prefixed with "i" or "a" to include the entire object (i == ""inner") or the entire object and
trailing whitespace (a == "all").

For example, `dip` will delete the entire current paragraph, including lines before the cursor.
Using `dap` will include the entire paragraph plus all whitespace lines below it.

One thing to note - combining these with commands like `c` and `d` means you can quickly
perform operations in a single step, like `dap` to cut a paragraph, instead of two in most
editors, like (1) select the paragraph and then (2) cut it.

* w: word - does not include dashes or underscores.
* W: WORD - this includes all non-whitespace, so includes underscores, but also parens, commas, etc.
* o: symbol - all characters the current mode considers a symbol, so dash is included in lisp
  but not parens.  If you are a programmer, this is the one you want to operate on variable names.
* s: sentence
* p: paragraph
* g: entire document.  This can only be used as "ig" and not "ag", which makes since since
  there is nothing after the entire document, I think it would be nice to map "ag" to the same
  thing.
* ' and ": use with 'i' for everything in quotes, 'a' to include the quotes
* (){}[]: use with 'i' for everything in these chars, 'a' to include the chars also
* t: Markup tags, like HTML or XML
* $: text from cursor to end-of-line
* f<c>: (find) text from cursor to character <c>.  Only works on same line.
* t<c>: ('til?) text from cursor to the character *before* <c> (e.g. delete until)
* gn: text-match (search results)
* i: indentation text object - all lines with same indentation (implemented via evil-indent-plus)
* I: indent, plus precending line with less indent ("def" in Python)
* /{regexp}, ?{regexp}: from cursor to the regular expression after / before.

The complete list is in [evil/evil-maps.el](https://github.com/emacs-evil/evil/blob/master/evil-maps.el#L280),
but other packages can add objects.  For example, the "gn" object requires the evil-search package.

Note that using the "v" command (visual mode) is very handy for selecting.  For example "vaW"
will select the current word (including underscores and dashes) the cursor is in.

See
[this article](https://medium.com/@schtoeffel/you-don-t-need-more-than-one-cursor-in-vim-2c44117d51db) for
an example of using the "gn" object.  The "gn" object is not supported natively - you'll need
to use the evil-search package.  See my init.el file.


## substitute

The substitute command (s) is handy for adding, changing, or deleting surrounding delimiters
such as quotes, parentheses, braces, brackets, backticks, and even asterisks.

* `s {add}`: To add a delimiter, first select some text then press `s`.  The cursor will turn
  into a half-block cursor, waiting for you to tell it the delimiter character to add.  So,
  `s"` will add double quotes around a selection and `s)` will add parentheses.
  
* `d s {delete}`: To delete an existing set of delimiters, use the delete command `d`.  With
  the cursor within double quotes, `d"` would delete them.  Likewise, `d(` or `d)` would delete
  surrounding parentheses.

* `c s {delete} {add}`: To replace existing delimiters, use the change command `c` and tell it
  what delimiter to delete and what to add.  For example, to change double quotes to single,
  you would use `cs"'`.

For delimiters that come in pairs such as `[]` and `()`, adding a left character will add it
with space inside but adding it with the right character will not add any space.  For
example, if you selected the text `x == 3` and added a left parenthesis using `s(` you would
end up with `( x == 3 )` but if you added a right, you would get `(x == 3)`.

I've found myself updating a lot of very old Python code where we used spaces after braces for
dictionaries: `{ key: val }`.  PEP8 specifies that the spaces should not be there.  To fix, I
can put the cursor anywhere within the braces and enter `cs{}`.  The left brace is what I
want to delete and since it is a left, it represents a delimiter *and* the space inside leaving
just `key: val`.  The right brace is what I want to add which is just a brace with no spaces,
so I am left with `{key: val}`

You can also treat XML and HTML tags as delimiters using the letter "t".  For example, if the
cursor is within an HTML span element, you can replace the span tags with div tags using
`cstt`.  This is the usual change command, but the first `t` tells Spacemacs to delete the
first set of tags it finds, which is the span.  The second `t` tells it to replace it with
another tag, so you are prompted for it in the minibuffer where you would enter `div`.  You can
also enter attributes such as `div class="test"` and the open tag would have the attributes but
the closing would not.

## registers

Registers are like named clipboards allowing you to store many snippets you can paste as
needed, without interfering with the main clipboard.  (If you are coming from vanilla emacs,
they are about the same as registers there.)  Check out
this [short article](https://gist.github.com/nifl/1178878) to see why you might want to use
them.

Double quotes followed by a character references a register.  Use this as a prefix to yank,
paste, and delete.

* "<char> y : Yank into register `char`
* "<char> d : Delete and copy into register `char`
* "<char> p : Paste from register `char`

The contents of registers are not lost when you use the clipboard normally.  Once you copy
something to register "x", it will be there until you replace it, so
you can paste it into multiple files or functions easily.

It's also nice that registers are named by a character so you can have many.  I use "a" for a
quick register since it is an easy character to hit.  But if I have something to copy into the
top of a bunch of functions and something to copy into the bottom, I may put one into 't' (top)
and the other into 'b' (bottom).

Remember to combine: `"xd2ap` to delete the next two paragraphs, plus the whitespace after the
second, into the "x" register.

# Cookbook

## Find Keybinding

If you know the standard Emacs command (or can guess it), use `SPC SPC` to display the commands
(like standard `M-x`); the bindings are shown for each command.

## Windows and Buffers

* `SPC w -`: split horizontally
* `SPC w 2` and `SPC w 3`: Split into 2 or 3 columns.
* `SPC w w`: switch among other buffers
* `SPC w m`: maximize the current window (eliminate splits)
* `SPC TAB`: switch to last buffer

Once split, each window will have a number in the left of its modeline. To jump
to another window, use `SPC <n>`

## Files

I'm using ivy (instead of helm) which provides completion.  One non-obvious feature is how to
create a new file instead of having it choose from existing ones.  While typing, close matches
are shown below and the first one is selected.  You can use Ctrl-k to move up from the
completion list to where you are typing and select *that*.

## Navigation

Use search (`/`) for efficient navigation, just like you would have with
isearch.

* `gg` and `G`: Go to beginning and end of document
* `(` and `)`: move by sentence
* `{` and `}`: move by paragraph
* `[[` and `[]`: move by section, which is often by function
* `%`: go to matching bracket, brace, parenthesis, etc.

Jumping:

* `SPC j j` and `SPC j w`: jump to character or word (avy functions)
* `SPC e p` and `SPC e n`: previous / next error
* `SPC j i`: jump to symbol in current file (imenu)
* `g ;`: go to last edit
* `ctrl-o`: jump back

To go to a specific line number, use `:<n>`.  If you don't have line numbers, or
if you can see the line you want, you might like `SPC j l` (avy jump to line).

## Clipboard

Use `y` to yank (copy) text to the clipboard, `d` to delete (cut), and `p` to paste.

Yank and delete are operators, so they needs to know what you want to operate on.  Use any of
the text objects: `yw` -> yank word, `yy` -> yank line, `y$` -> yank to end of line, etc.  As
with most commands, if you have a selection you made using visual mode (pressing `v` and moving
around for instance), just pressing `y` will yank since it already knows what you want to
operate on.

`p` will paste after the cursor and `P` will paste before.  If you copied lines,
it will paste on the next line, otherwise on the same line after the cursor.

* `y`: yank
* `p` and `P`: paste after and before
* `SPC r y`: show kill ring

Use [registers](#registers) to store a copy of text and paste it multiple times:

* `"qdip` - delete (cut) paragraph to register "q"
* `"qy` - paste from register q

Once you have pasted, `viP` will select the pasted text.

## Insert

I believe these are standard VI:

* `i` or `a`: insert at or one character forward (insert or append)
* `r`: replace character under cursor
* `A`: append to line (jump to end and then append)
* `o` and `O`: open line below / above current line

The `c` command (change / correct) accepts a text object - it deletes the object
and then starts insert mode: `cw` -> correct word, `ci(` -> correct in parens,
etc.

### Column Editing

* `S-i`: insert at beginning of each line
* `S-a`: append to each line

To insert characters at the beginning of each line, select the lines and press Shift-I.  This
will put you in insert mode on the first line.  Type what you want then press Esc - at that
point what you typed will be inserted at the beginning of the other lines.

## Searching

* `/` and `?`:  start incremental search, forward and backward.  This is a regex
* `*` and `#`: find next / previous instance of word the cursor is over
* `SPC /`: search project
* `SPC *`: search project for current word

After searching, the search term remains highlighted, but if they get annoying, clear them with
`SPC s c`.

* `n` and `N`: go to next / previous search occurrence

I've configured evil-search, so it may behave differently than regular spacemacs documentation.
I've done so because it supports the 'gn' text object.  When that is added to core spacemacs
I'll get rid of it.

## Search and Replace

### ex command

You can use the `:s` substitute command to replace items on the current or selected lines:
`:s/foo/bar/`.  If you want to replace the same thing you replaced in the previous incantation,
leave out the search text: `:s//xyzzy/`.  Also, the final "/" is not necessary if you aren't
going to pass additional flags like "c" and "g".

By default, the command only replaces the first occurance.  You can append a 'g' (global) to
replace all: `:s/foo/bar/g`.  However, there is a Spacemacs setting to reverse the meaning of
'g' and have the default replace all, which is what I've enabled.

If you append a `c`, it will ask for confirmation for each replacment: `:s/foo/bar/c`.

If you want to replace all in the documents, you can select the entire document first with
`vig` or just use `:%s/foo/bar`.

#### Reselect

When you perform an operation, such as replace, in the visual mode selection, the selection
is then removed.  You can reselect the last selection using `gv` (evil-visual-restore).

## grep

There are a lot of options (ag, etc.), but the main thing I want is a simple grep interface.
(Later I'll need to add wgrep.)  If I use `SPC g s p` (counsel-grep-project) it displays a live
menu to choose one entry to jump to.  To keep the list to refer to later, use `ctrl-o o` to
turn it into an occur buffer.  (The `ctrl-o` part is a standard Ivy keybinding to show a menu
of actions for the list.)

I *highly* recommend you install rg, however.  It is super fast.

## Selecting

### Visual Mode

Turn on visual mode using `v`. This is liking dropping a mark in standard Emacs - wherever you
move to it creates a highlighted region.

To select larger items, use 'inner" text objects like `vip` to select a paragraph or `vis` to
select a sentence. For programming, selecting within parentheses or quotes using `vi(` and
`vi"`.

### Expand Region

This is one of the handiest. Press `SPC v` to highlight the word around the
cursor. This starts a transient state that shows the available keys in the mode
line. Pressing `v` again expands the selection again. If you overshoot, `V`
decreases it.

Note that this command is smart enough to increase to all text within quotes or
parens, matching delimiters, and even function definitions.

### Select In Parens

As with many things in emacs, there will be a few ways todo this. Here are two that work when
you are inside the parens:

1. Use expand-region by repeatedly pressing `SPC v`.
2. Use the "inner parentheses" text object by pressing `vi(`

To include the parens, use expand-region. If using (1), just press `v` again. If
using (2), use `va(` instead.

## Moving Text

Once you've selected text, use `SPC x J` or `SPC x K` to move it up or down.
This starts a transient state allowing you to continue pressing J and K to move
the text up and down more.  Note that both are capital letters.

**IMPORTANT:** This seems to have a bug if there is more than one line.

To indent left and right, use `SPC x TAB` to start an "indent rigidly" transient state.  Use
'j' and 'h' to move left and right by one and 'J' and 'H' to move by more.  (It is two in my
scratch buffer, but I would be surprised if it wasn't mode specific.)

## Changing Case

* U : uppercase
* u : lowercase
* ~ : toggle case

With no selection, these operate on the character under the cursor and move forward.

You can use the "g" prefix (that might not be the right term) which requires a subject, such as
`gUiW` to upper case the current WORD.

* gU{motion/object}
* gu{motion/object}
* g~{motion/object}

Unfortunately these don't work when the cursor is right after a word, which is when I'd want to
uppercase a constant if I didn't have capslock on.  You can use '-', but it seems to operate on
the entire paragraph.  This needs more research.

## Marks

https://gist.github.com/nifl/1178878

Spacemacs (evil, really) implements vi's marks:

* `m <char>` : Mark the cursor location with the given character.
* `' <char>` : Go to the line with the specified mark
* `` <char>` : Go to the specified mark

The apostrophe is a motion command, so you can compose it such as: `y'a` which yanks (y
command) from the cursor to the mark "a" ('a motion).

## Macros

q{char} to record a macro and name it after the character.  q to stop recording.  @{char} to replay it.

# Outstanding Questions

A collection of items I need to figure out how to do in Spacemacs. Some may simply require me
to install the same package I used in standard Emacs. Some I just need to look up the key
bindings for.

* hide .pyc from completion
* how to access history in counsel-find-file (SPC f f)
* counsel-imenu jumps to a symbol at the bottom of the screen.  Would like top
  or at least middle.
* How to renumber a column of numbers (or start a column)?  I used to use CUA
  rectangles for this.
* symbol text-object ('o') in Python selects too much -  includes colon at end of if statement

# Multiple Cursors

evil-mc

* grm - evil-mc-make-all-cursors
* gru - evil-mc-undo-all-cursors
* grs - evil-mc-pause-cursors
* grr - evil-mc-resume-cursors
* grf - evil-mc-make-and-goto-first-cursor
* grl - evil-mc-make-and-goto-last-cursor
* grh - evil-mc-make-cursor-here
* grj - evil-mc-make-cursor-move-next-line
* grk - evil-mc-make-cursor-move-prev-line
* M-n - evil-mc-make-and-goto-next-cursor
* grN - evil-mc-skip-and-goto-next-cursor
* M-p - evil-mc-make-and-goto-prev-cursor
* grP - evil-mc-skip-and-goto-prev-cursor
* C-n - evil-mc-make-and-goto-next-match
* grn - evil-mc-skip-and-goto-next-match
* C-t - evil-mc-skip-and-goto-next-match
* C-p - evil-mc-make-and-goto-prev-match
* grp - evil-mc-skip-and-goto-prev-match
