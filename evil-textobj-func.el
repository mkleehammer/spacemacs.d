;; Create a text object for functions tied to "f".  (This is the same as "find", so use as
;; "if" and "af".)  Unfortunately I'm not sure how to easily, and cross-mode, select "stuff"
;; that goes with the function like preceding comments or decorators.

(require 'evil)

(defgroup evil-textobj-func nil
  "Evil text object for functions"
  :prefix "evil-textobj-func-"
  :group 'evil)

(defcustom evil-textobj-func-key "f"
  "Key for evil function text object"
  :type 'string
  :group 'evil-textobj-func)

(defun evil--sel-func (outer)
  (let (beg end)
    (save-excursion
      (beginning-of-defun)
      (setq beg (point))
      (end-of-defun)
      (while (and outer (looking-at "[:space:]*$"))
        (forward-line))
      (setq end (point)))
    (evil-range beg end))
  )

(evil-define-text-object evil-inner-func (count &optional beg end type)
  "Select current function"
  (evil--sel-func nil))

(evil-define-text-object evil-outer-func (count &optional beg end type)
  "Select current function"
  (evil--sel-func t))

(define-key evil-inner-text-objects-map evil-textobj-func-key 'evil-inner-func)
(define-key evil-outer-text-objects-map evil-textobj-func-key 'evil-outer-func)

(provide 'evil-textobj-func)
